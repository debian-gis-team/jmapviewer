#!/bin/sh
# Simple wrapper script used to start JMapViewer_Demo in Debian
set -e

java -Djava.net.useSystemProxies=true -jar /usr/share/jmapviewer/JMapViewer_Demo.jar "$@"

