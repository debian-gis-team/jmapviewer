Source: jmapviewer
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Felix Natter <fnatter@gmx.net>,
           David Paleino <dapal@debian.org>,
           Andreas Tille <tille@debian.org>,
           Bas Couwenberg <sebastic@debian.org>
Section: utils
Priority: optional
Build-Depends: debhelper-compat (= 13),
               default-jdk (>= 2:1.8) | java8-sdk,
               ant,
               docbook-xsl,
               docbook-xml,
               xsltproc
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-gis-team/jmapviewer
Vcs-Git: https://salsa.debian.org/debian-gis-team/jmapviewer.git
Homepage: https://wiki.openstreetmap.org/wiki/JMapViewer
Rules-Requires-Root: no

Package: jmapviewer
Architecture: all
Depends: default-jre (>= 2:1.8) | java8-runtime,
         ${misc:Depends}
Breaks: freeplane (<< 1.3.15-3~)
Description: Java OpenStreetMap Tile Viewer
 JMapViewer is a Java Swing component for integrating OSM maps in to your Java
 application. JMapViewer allows you to set markers on the map or zoom to a
 specific location on the map.
 .
 This package includes both the library and a demo application.
