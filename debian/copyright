Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: JMapViewer
Source: https://svn.openstreetmap.org/applications/viewer/jmapviewer
Comment: The upstream source is repacked to exclude the Bing logo which is
 non-free, see: https://lists.debian.org/debian-gis/2014/10/msg00147.html
Files-Excluded: src/org/openstreetmap/gui/jmapviewer/images/bing_maps.png

Files: *
Copyright: 2007, Tim Haussmann
           2009, Stefan Zeller
           2009, Karl Guggisberg
           2009, Dave Hansen
      2010-2011, Ian Dees
      2010-2011, Michael Vigovsky
           2011, Jason Huntley
      2008-2012, Jan Peter Stotz
           2012, Teemu Koskinen
           2012, Jiri Klement
           2013, Matt Hoover
           2013, Alexei Kasatkin
           2013, Galo Higueras
      2011-2016, Gleb Smirnoff
      2011-2017, Paul Hartmann
           2017, Robert Scott
      2015-2019, Wiktor Niesiobędzki
      2012-2020, Simon Legner
      2009-2022, Dirk Stöcker
      2011-2022, Vincent Privat
License: GPL-2+

Files: src/org/openstreetmap/gui/jmapviewer/tilesources/ScanexTileSource.java
Copyright: 2011-2016, Gleb Smirnoff & Andrey Boltenkov
License: BSD-2-Clause or GPL-2+

Files: debian/*
Copyright: 2011-2013, Andrew Harvey <andrew.harvey4@gmail.com>
                2013, Felix Natter <fnatter@gmx.net>
License: BSD-2-Clause or GPL-2+

License: BSD-2-Clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
 .
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the
     distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

